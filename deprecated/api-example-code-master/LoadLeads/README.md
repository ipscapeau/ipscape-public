# Introduction to LoadList

LoadList is example PHP code that provides the logic to import a list of leads into one or more pre-existing lists in the ipSCAPE Cloud Contact Centre System. It is intended as an example of how this functionality can be implemented using the ipSCAPE API, rather than as a real-life implementation.

# Prerequsites

- A user account in the Workspace of the relevant ipSCAPE tenant
- An API key for access to the Workspace of the relevant ipSCAPE tenant

# Usage

The application is invoked as follows:

`php LoadList.php <INI file name> [<Lead List CSV filename>]`

where:
- The mandatory INI file name defines the set of user credentials needs to identify the target Workspace, and the user credentials
- The optional CSV file name provides the details of the Leads to be imported. If this is absent, the default filename of "TestLeadList.csv" will be used.

## INI file format

The INI file contains 4 lines that define the essential credentials needed to log into the API server for a particular tenant, and issue API request. Those credentials are:

| Parameter Name | Purpose |
| -------------- | ------- |
| baseURL | Defines the full API server URL. |
| userId | Identifies the user under whose account API operations will be performed. |
| password | Defines the password of the identifed user. |
| apiKey | Defines the API key to be used for this application. |

An example file is:

```
baseURL=https://xyz.ipscape.com.au/api/v1.0.0/
userId=testuser
password=12345678
apiKey=01234567891ab
```

## CSV file format

The purpose of this example application is to import a set of leads from a CSV file into the relevant List of a Campaign in the user's Workspace. All details of the Leads are defined in the CSV file, which must have a first row containing field names (which are case-independent). In general, the field names must be identical to those used within the application (ie Custom Data Field names), with a few exceptions:

| Field Name | Meaning |
| ---------- | ------- |
| campaignId | The numeric Id of the Campaign in which the relevant List is located. This is used by the API as the unique identifier of the Campaign. |
| campaignTitle | The alphanumeric title of the Campaign in which the relevant List is located. Typically, this will be more readily known to users of UI than the Campaign Id. So, it will generally be easier to provide the title than the id. Only one of them needs to be provided. |
| listId | The numeric Id of the List into which the leads will be loaded. This is used by the API as the unique identifier of the List. |
| listTitle | The alphanumeric title of the List into which the leads will be loaded. Typically, this will be more readily known to users of UI than the Lead Id. So, it will generally be easier to provide the title than the Id. Only one of them needs to be provided. |
| PhoneNumber1 | The first phone number for the lead (referred to as "Phone1" in the UI) |
| PhoneNumber2 | The second phone number for the lead (referred to as "Phone2" in the UI) |
| PhoneNumber3 | The third phone number for the lead (referred to as "Phone3" in the UI) |
| leadTimezone | The timezone of the lead (referred to as "Timezone" in the UI) |
| customerKey | The (optionally) unique idenitifer of the lead (referred to as "Customer Key" in the UI) |
| doUpdate | The update flag for this lead. This is a boolean field. If set to 1 (TRUE) and the input Customer Key matches the Customer Key of a lead that is already present in the List, then the existing Lead details are updated. If not, a new lead is created, potentially with a duplicate Customer Key. |
| earliestCallTimestamp | The earliest time of day that the lead may be called, in the Lead's timezone. A valid date/time format must be used (e.g. yyyy-mm-dd hh:mm:ss). The default is now. |
| leadType | The type of Lead, which can be any one of: 'New', 'Call Back - Agent', 'Call Back - Queue'. The default is 'New'. |

An example 2-line CSV file is:
```
CampaignTitle,ListTitle,FirstName,LastName,PhoneNumber1,PhoneNumber2,PhoneNumber3,leadTimezone,doUpdate
Load Leads Test 1,List 1,John,Smith,+61432101234,+61298765432,,Australia/Sydney,0
```
Note that this CSV file could only be imported into a Campaign that had "FirstName" and "LastName" custom data fields (at least) defined.

## Dependencies

The modules in this application include modules from the [Common](https://github.com/ipSCAPE/api-example-code/tree/master/Common) ipSCAPE GitHub repository.

# Licence

This project has been released under a modified version of the [MIT License](https://opensource.org/licenses/MIT), the text of which is included below. The only modification is the removal of the obligation to include the standard permission notice in any copies of the Software. This license applies ONLY to the source of this repository and does not extend to any other ipSCAPE distributions or variants, or any other 3rd party libraries used in a repository.

> Copyright (c) 2016 ipSCAPE Pty Limited.

> Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
  to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
  and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so.

> THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
  IN THE SOFTWARE.
