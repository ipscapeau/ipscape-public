<?php

require_once dirname(__FILE__) . '/LoadLeadList.php';
require_once dirname(__FILE__) . '/../Common/SendRequest.php';
require_once dirname(__FILE__) . '/../Common/Session.php';

/*
 * Module:          LoadList.php
 *
 * Purpose:         Provides the top-level logic to load a list from a CSV file into one or more campaign lead list(s).
 *                  It is intended as an example of how this functionality can be implemented using the ipSCAPE API, rather than as a real-life implementation.
 *                  The format of the CSV file is that it contains a header row, which defines the data field names. Then one or more data rows follow, with the
 *                  data values of each of the data fields. An example might be:
 *
 *                      CampaignTitle,ListTitle,FirstName,LastName,PhoneNumber1,PhoneNumber2,PhoneNumber3,leadTimezone,doUpdate
 *                      Load Leads Test 1,List 1,Simon,Laing,+61438503427,+61289993138,,Australia/Sydney,0
 *
 * Author:          Simon Laing
 *
 * Copyright:       Copyright (c) 2016 ipSCAPE Pty Limited.
 *
 *                  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 *                  to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 *                  and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so.
 *
 *                  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *                  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *                  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 *                  IN THE SOFTWARE.
 *
 * Revision History
 *      11/1/2016   SGL     First version.
 */


$logLevel = 2;                                      // Logging granularity: 0 = No logging, 1 = Errors only, 2 = Debug, 3 = cURL debug
$filename = "TestLeadList.csv";                     // Default CSV lead list file to be loaded

// Parse the command line, looking for the filename of the ini file containing the operational parameters
if ($argc < 2) {
    echo "Syntax is: " . $argv[1] . " <INI file name> [<Lead List CSV filename>]\n";
    exit;

} else {
    // Attempt to open the ini file, and extract the operational parameters
    $params = parse_ini_file($argv[1]);
    if (!$params) {
        // Failed to parse the INI file, so force an exit
        if ($logLevel >= 1) {
            echo "LoadList: ERROR - Failed to parse INI file: " . $argv[1] . "\n";
        }

    } else {
        // If it has been provided, pick up the name of the CSV file containing the leads to be imported
        if ($argc > 2) {
            $filename = $argv[2];
        }

        // Attempt to log in to the API
        $response = apiLogin($params['baseURL'], $params['userId'], $params['password'], $params['apiKey'], $logLevel);

        if ($response != '') {
            // Logged in successfully, so go to work
            // Load the list, picking all of the details out of the CSV file
            loadLeadList($params['baseURL'], $params['userId'], $params['password'], $filename, $logLevel);

            // Now log out of the API
            apiLogout($params['baseURL'], $params['userId'], $params['password'], $logLevel);
        }
    }
}

?>
