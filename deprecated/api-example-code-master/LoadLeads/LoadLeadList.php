<?php

require_once dirname(__FILE__) . '/../Common/SendRequest.php';
require_once dirname(__FILE__) . '/../Common/Session.php';
require_once dirname(__FILE__) . '/../Common/Utils.php';

/*
 * Module:          LoadLeadList.php
 *
 * Purpose:         Provides the logic to import a list of leads into one or more pre-existing lists in the system. It is intended as an example of how this functionality
 *                  can be implemented using the ipSCAPE API, rather than as a real-life implementation.
 *
 * Author:          Simon Laing
 *
 * Copyright:       Copyright (c) 2016 ipSCAPE Pty Limited.
 *
 *                  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 *                  to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 *                  and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so.
 *
 *                  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *                  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *                  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 *                  IN THE SOFTWARE.
 *
 * Revision History
 *      11/1/2016   SGL     First version.
 */

/*
 * Function:        loadLeadList
 *
 * Purpose:         Main function, that opens the CSV-format input file, parses it, extracts lead details one row at a time, and imports them
 *                  into the nominated list. The first row of the input file must be a header row, containing the names of the fields in the rows
 *                  that follow. Some of these fields are mandatory, but the rest are optional. Although some of them are standard (that is, they exist
 *                  in every campaign by default), others can be custom data fields, which can have any names the user likes. Any fields which are empty
 *                  will be ignored.
 *
 *                  The main parameters required to load a lead are Campaign Id and List Id. However, since these are fairly internal numeric ids, it is
 *                  generally easier to identify these entities with a Campaign Title and List Title respectively. So, in both cases, if the Id is provided,
 *                  it is used. But, if a Title is provided, a lookup is performed to determine the Id, before loading the lead. So the mandatory data fields are:
 *
 *                      CampaignId or CampaignTitle
 *                      ListId or ListTitle
  *
 *                  And, the standard data fields are:
 *
 *                      agentId         The ID number of the agent (e.g. 1234). If the Lead Type is 'Call Back - Agent', this field is mandatory.
 *                      customerKey     The customer key associated with the lead list when it was imported.
 *                      doUpdate        This is a boolean field. If set to 1 (yes) and Customer key is matched, then the record is updated; otherwise a new lead is created. [Select 1 or 0]. Default = 1.
 *                      earliestCallTimestamp   The earliest time of day that the lead may be called. Use a valid date/time format (e.g. yyyy-mm-dd hh:mm:ss). The default is current timestamp.
 *                      leadData        Any additional lead data fields, with each field name used as the key and the contents of that field as its value.
 *                                      [Example format: 'firstname: John, lastname: Snow, address: 1 Walker St, state: NSW, postcode: 2052'].
 *                      leadTimezone    The timezone in which this lead is resident. Default is the timezone set for the Contact Call Time Window in the campaign's settings.
 *                      leadType        The type of Lead. Select one of: 'New', 'Call Back - Agent', 'Call Back - Queue'. Default is 'New'.
 *                      phoneNumber1    A phone number associated with the lead, Valid phone number (E.164), e.g. (02)84930293, +61473829344, 1300493829, 83748392.
 *                      phoneNumber2    A phone number associated with the lead, Valid phone number (E.164), e.g. (02)84930293, +61473829344, 1300493829, 83748392.
 *                      phoneNumber3    A phone number associated with the lead, Valid phone number (E.164), e.g. (02)84930293, +61473829344, 1300493829, 83748392.
 *
 * Parameters:      $baseURL        Base URL for the API server
 *                  $userId         User Id of the caller (if blank, no authentication is performed)
 *                  $password       Authentication password of the caller
 *                  $filename       Filename and path of the leads list, in CSV file format
 *                  $logLevel       Logging level setting. 0 = No logging, 1 = Errors only, 2 = Debug, 3 = cURL debug
 *
 * Returns:         Nothing
 *
 */
function loadLeadList($baseURL, $userId, $password, $filename, $logLevel)
{
    // Open the CSV file
    $header = NULL;
    $leadDetails = array();
    $loopFlag = TRUE;

    // Loop until the end of the file, or a fatal error occurs
    if (($handle = fopen($filename, 'r')) !== FALSE) {
        // Read in a line, and parse it into an array, using commas as delimiters
        while ((($row = fgetcsv($handle, 1000, ',')) !== FALSE) && ($loopFlag !== FALSE)) {
            unset($leadDetails);
            unset($rawLeadDetails);

            // If this is the first row, it must be the header row
            if (!$header) {
                $header = $row;

                // Convert all the field names to lower case, to simplify future comparisons
                for ($i = 0; $i < count($header); $i++) {
                    $header[$i] = strtolower($header[$i]);
                }

            } else {
                // This is a data row. Check whether it's complete. If it isn't, ignore it
                if (count($row) !== count($header)) {
                    if ($logLevel >= 1)
                        echo "loadLeadList: ERROR - Input row doesn't have the correct number of data fields\n";

                } else {
                    // This is a complete data row, so put it into an array with the field names as keys
                    $leadDetails = array_combine($header, $row);

                    // If a Campaign Id has been given, use it. If not, look it up from the Campaign Title.
                    $campaignId = 0;
                    if (array_key_exists('campaignid', $leadDetails) && ($leadDetails['campaignid'] > 0)) {
                        // Pick up the campaign id directly from the input data
                        $campaignId = $leadDetails['campaignid'];
                    } else {
                        // We don't have the Campaign Id, but do we have a Campaign Title?
                        if (array_key_exists('campaigntitle', $leadDetails)) {
                            // We have the Campaign Title, so look up the Campaign Id
                            $campaignId = getCampaignId($baseURL, $userId, $password, $leadDetails['campaigntitle'], $logLevel);

                        } else {
                            // We don't have either the Campaign Id or the Campaign Title, so we can't proceed
                            if ($logLevel >= 1)
                                echo "loadLeadList: ERROR - Neither Campaign Title nor Campaign Id were provided.\n";
                        }
                    }

                    // If a List Id has been given, use it. If not, look it up from the List Title.
                    $listId = 0;
                    if ($campaignId != 0) {
                        if (array_key_exists('listid', $leadDetails) && ($leadDetails['listid'] > 0)) {
                            // Pick up the list id directly from the input data
                            $listId = $leadDetails['listid'];

                        } else {
                            // We don't have the List Id, but do we have a List Title?
                            if (array_key_exists('listtitle', $leadDetails)) {
                                // We have the List Title, so look up the List Id
                                $leadDetails['listid'] = getListId($baseURL, $userId, $password, $campaignId, $leadDetails['listtitle'], $logLevel);

                            } else {
                                // We don't have either the List Id or the List Title, so we can't proceed
                                if ($logLevel >= 1)
                                    echo "loadLeadList: ERROR - Neither List Title nor List Id were provided.\n";
                            }
                        }
                    }

                    // If we have both the Campaign Id and the List Id, attempt to load this lead's details
                    if (($campaignId != 0) && ($listId != 0)) {
                        // If a Campaign or List Title is present, then remove it, otherwise we'll try and load this data into a custom data field
                        if (array_key_exists('campaigntitle', $leadDetails))
                            unset($leadDetails['campaigntitle']);
                        if (array_key_exists('listtitle', $leadDetails))
                            unset($leadDetails['listtitle']);

                        // Set up the Camapign and List Ids, then try and load this lead
                        $leadDetails['campaignid'] = $campaignId;
                        $leadDetails['listid'] = $listId;
                        loadLead($baseURL, $userId, $password, $leadDetails, $logLevel);
                    }
                }
            }
        }

        // Close the input file prior to exit
        fclose($handle);

    } else {
        // Failed to open input file
        if ($logLevel >= 1)
            echo "loadLeadList: ERROR - Failed to open input file: " . $filename . "\n";
    }
}

/*
 * Function:        loadLead
 *
 * Purpose:         Load a single lead into the nominated campaign and list.
 *
 * Parameters:      $baseURL        Base URL for the API server
 *                  $userId         User Id of the caller (if blank, no authentication is performed)
 *                  $password       Authentication password of the caller
 *                  $leadDetails    Assocative array of lead details
 *                  $logLevel       Logging level setting. 0 = No logging, 1 = Errors only, 2 = Debug, 3 = cURL debug
 *
 * Returns:         On success, the data string returned from the remote server. On error, an empty data string.
 *
 */
function loadLead($baseURL, $userId, $password, $leadDetails, $logLevel)
{
    $curlURL = $baseURL . 'lead/createlead/';
    $definedFields = array("agentid", "campaignid", "customerkey", "doupdate", "earliestcalltimestamp", "leaddata", "leadtimezone", "leadtype", "listid", "phonenumber1", "phonenumber2", "phonenumber3");
    $customData = '';
    $leadData = array();

    // Go through the array, building up the parameter string. The defined (ie standard) fields each have keys of their own,
    // but the custom data fields are included in a single "leaddata" field.
    foreach ($leadDetails as $key => $value) {
        // Skip over null values, if they exist
        if (strlen(trim($value)) > 0) {
            if (array_search($key, $definedFields)) {
                // This is a standard field, so add it in with its own field name
                $leadData[$key] = $value;

            } else {
                // This is a custom data field, so add it into the separate array
                if (strlen($customData) > 0)
                    $customData .= ",";
                $customData .= $key . ":" . $value;
            }
        }
    }

    // All done, so finally add in the custom data, if there is any
    if (strlen($customData) > 0) {
        $leadData['leaddata'] = $customData;
    }

    // Send the final request to the API server
    $response = sendRequest($curlURL, 'POST', $userId, $password, $leadData, $logLevel);

    if ($response != '') {
        // Managed to load the lead. Decode the response.
        $responseObject = json_decode($response, TRUE);

        // Depending on the result, and the current log level, report appropriately
        if ($responseObject ['resultCode'] = 'success') {
            if ($logLevel >= 2) {
                echo "loadLead: DEBUG - Successfully created lead:\n";
                print_r($responseObject);
            }
        } else {
            if ($logLevel >= 1) {
                echo "loadLead: ERROR - Unsuccessful attempt to create lead:\n";
                print_r($responseObject);
            }
        }
    } else {
        // Failed to load this lead
        if ($logLevel >= 1)
            echo "loadLead: ERROR - Failed to load Lead\n";
    }

    return $response;
}

?>
