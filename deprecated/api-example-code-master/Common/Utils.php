<?php

require_once dirname(__FILE__) . '/SendRequest.php';

/*
 * Module:          Utils.php
 *
 * Purpose:         Provides some utility functions to perform commonly required API operations.
 *
 * Author:          Simon Laing
 *
 * Copyright:       Copyright (c) 2016 ipSCAPE Pty Limited.
 *
 *                  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 *                  to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 *                  and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so.
 *
 *                  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *                  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *                  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 *                  IN THE SOFTWARE.
 *
 * Revision History
 *      22/1/2016   SGL     First version.
 */

/*
 * Function:        getCampaignId
 *
 * Purpose:         To return the Campaign Id from the Campaaign Title. The Campaign Id is the primary campaign identifier used by
 *                  API calls, but it is fairly hidden within the UI, whereas the Campaign Title is well-known and displayed.
 *
 * Parameters:      $baseURL        Base URL for the API server
 *                  $userId         User Id of the caller (if blank, no authentication is performed)
 *                  $password       Authentication password of the caller
 *                  $campaignTitle  Title (ie Name) of the campaign whose id is required
 *                  $logLevel       Logging level setting. 0 = No logging, 1 = Errors only, 2 = Debug, 3 = cURL debug
 *
 * Returns:         Campaign Id, or 0 if the campaign doesn't exist
 *
 */
function getCampaignId($baseURL, $userId, $password, $campaignTitle, $logLevel)
{
    $campaignId = 0;
    $curlURL = $baseURL . 'campaign/readcampaignslist/';
    $curlData = 'pageNo=1&perPage=1&campaignTitle=' . urlencode($campaignTitle);

    // Send the request to the API server
    $response = sendRequest($curlURL, 'GET', $userId, $password, $curlData, $logLevel);
    if ($response != '') {
        // Managed to send the request. Decode the reply.
        $responseObject = json_decode($response, TRUE);
        if ($logLevel >= 2) {
            echo "getCampaignId: DEBUG - Response to readCampaignsList:\n";
            print_r($responseObject);
        }

        if ($responseObject['resultCode'] === 'success') {
            // The request was successful, and the requested campaign was found. Pick up its Id
            $campaignId = $responseObject['result']['data'][0]['campaignId'];
        }
    }

    return $campaignId;
}

/*
 * Function:        getListId
 *
 * Purpose:         To return the List Id from the List Title. The List Id is the primary list identifier used by
 *                  API calls, but it is fairly hidden within the UI, whereas the List Title is well-known and displayed.
 *
 * Parameters:      $baseURL        Base URL for the API server
 *                  $userId         User Id of the caller (if blank, no authentication is performed)
 *                  $password       Authentication password of the caller
 *                  $campaignId     Id of the campaign
 *                  $listTitle      Name of the list whose id is required
 *                  $logLevel       Logging level setting. 0 = No logging, 1 = Errors only, 2 = Debug, 3 = cURL debug
 *
 * Returns:         List Id, or 0 if the List doesn't exist
 *
 */
function getListId($baseURL, $userId, $password, $campaignId, $listTitle, $logLevel)
{
    $listId = 0;
    $curlURL = $baseURL . 'lead/readleadslist/';
    $curlData = 'pageNo=1&perPage=1&campaignid=' . $campaignId . '&listTitle=' . urlencode($listTitle);

    // Send the request to the API server
    $response = sendRequest($curlURL, 'GET', $userId, $password, $curlData, $logLevel);
    if ($response != '') {
        // Managed to send the request. Decode the reply.
        $responseObject = json_decode($response, TRUE);
        if ($logLevel >= 2) {
            echo "getListId: DEBUG - Response to readLeadsList:\n";
            print_r($responseObject);
        }

        if ($responseObject['resultCode'] === 'success') {
            // The request was successful, and the requested list was found. Pick up its Id
            $listId = $responseObject['result']['data'][0]['listId'];
        }
    }

    return $listId;
}


?>
