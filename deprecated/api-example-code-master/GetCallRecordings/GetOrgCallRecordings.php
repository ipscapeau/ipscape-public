<?php

require_once dirname(__FILE__) . '/GetCallRecordings.php';
require_once dirname(__FILE__) . '/../Common/SendRequest.php';
require_once dirname(__FILE__) . '/../Common/Session.php';

/*
 * Module:          GetOrgCallRecordings.php
 *
 * Purpose:         Provides the top-level logic to retrieve all call recordings, for one organisation, from all campaigns, made within the last n seconds. This module
 *                  is intended as an example of how this functionality can be implemented using the ipSCAPE API, rather than as a real-life implementation. In particular,
 *                  the various operational paramters (n, user ids, passwords, etc) would typically be parameters that would either be provided when the application
 *                  is run, or read from some other source (configuration file or database, for example).
 *
 * Author:          Simon Laing
 *
 * Copyright:       Copyright (c) 2016 ipSCAPE Pty Limited.
 *
 *                  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 *                  to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 *                  and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so.
 *
 *                  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *                  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *                  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 *                  IN THE SOFTWARE.
 *
 * Revision History
 *      3/1/2016    SGL     First version.
 */

$logLevel = 2;                                      // Logging granularity: 0 = No logging, 1 = Errors only, 2 = Debug, 3 = cURL debug
$orgTimezone = "Australia/Sydney";                  // Timezone for the organisation (which is used for setting search period)
// $duration = (1 * 60 * 60);                     // Period, in seconds before now, for which call recordings are required.
$duration = (1 * 24 * 60 * 60);                     // Period, in seconds before now, for which call recordings are required.
$baseDir = "D:\\Dev\Data\\recordings\\";            // Base directory path, in which the recordings will be stored. This directory MUST exist!

// Parse the command line, looking for the filename of the ini file containing the operational parameters
if ($argc != 2) {
    echo "Syntax is: " . $argv[1] . " <ini file name>\n";
    exit;

} else {
    // Attempt to open the ini file, and extract the operational parameters
    $params = parse_ini_file($argv[1]);
    if (!$params) {
        // Failed to parse the INI file, so force an exit
        if ($logLevel >= 1) {
            echo "GetOrgCallRecordings: ERROR - Failed to parse INI file: " . $argv[1] . "\n";
        }

    } else {
        // All OK, so attempt to log in to the API
        $response = apiLogin($params['baseURL'], $params['userId'], $params['password'], $params['apiKey'], $logLevel);

        if ($response != '') {
            // Logged in successfully, so go to work
            // Work out the start and end times of the period for which call recordings are required.
            date_default_timezone_set($orgTimezone);                          // All times are interpreted in the Org Timezone
            $end = time();
            $endTime = urlencode(strftime("%F %T", $end));                    // Timestamp for the end of the retrieval period
            $startTime = urlencode(strftime("%F %T", $end - $duration));     // Timestamp for the start of the retrieval period

            // Get all call recordings, for all campaigns, made during this period
            getAllCallRecordings($params['baseURL'], $params['userId'], $params['password'], $startTime, $endTime, $baseDir, $logLevel);

            // Now log out of the API
            apiLogout($params['baseURL'], $params['userId'], $params['password'], $logLevel);
        }
    }
}

?>
