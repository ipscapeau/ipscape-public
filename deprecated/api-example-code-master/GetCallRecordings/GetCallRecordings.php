<?php

require_once dirname(__FILE__) . '/../Common/SendRequest.php';

/*
 * Module:          GetCallRecordings.php
 *
 * Purpose:         Provides the complete logic to retrieve all call recordings, for one organisation, for all campaigns, made within the last n seconds. This module
 *                  is intended as an example of how this functionality can be implemented using the ipSCAPE API, rather than as a real-life implementation.
 *
 * Author:          Simon Laing
 *
 * Copyright:       Copyright (c) 2016 ipSCAPE Pty Limited.
 *
 *                  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 *                  to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 *                  and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so.
 *
 *                  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *                  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *                  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 *                  IN THE SOFTWARE.
 *
 * Revision History
 *      3/1/2016    SGL     First version.
 */

/*
 * Function:        getAllCallRecordings
 *
 * Purpose:         Main function, that iterates through all campaigns, retrieving call recordings for each
 *
 * Parameters:      $baseURL        Base URL for the API server
 *                  $userId         User Id of the caller (if blank, no authentication is performed)
 *                  $password       Authentication password of the caller
 *                  $startTime      Start timestamp
 *                  $endTime        End timestamp
 *                  $baseDir        Base of directory tree in which to store the recordings
 *                  $logLevel       Logging level setting. 0 = No logging, 1 = Errors only, 2 = Debug, 3 = cURL debug
 *
 * Returns:         Nothing
 *
 */
function getAllCallRecordings($baseURL, $userId, $password, $startTime, $endTime, $baseDir, $logLevel)
{
    // Access each of this organisation's campaigns in turn.
    // The readCampaignsList API function is designed to return campaign details in pages, with a user-defined length. This is
    // useful when building web pages to view the data, but for our purposes, it is more convenient to cycle through the campaigns
    // one at a time. This is simply achieved by setting the page size to 1, and using the page number as an incrementing index.
    $curlURL = $baseURL . 'campaign/readcampaignslist';
    $pageNo = 0;
    $exitFlag = FALSE;
    do {
        $pageNo++;
        $curlData = 'pageNo=' . $pageNo . '&perPage=1';
        $response = sendRequest($curlURL, 'GET', $userId, $password, $curlData, $logLevel);
        if ($response != '') {
            // Managed to retrieve the campaign details. Decode them.
            $responseObject = json_decode($response, TRUE);
            if ($logLevel >= 2) {
                echo "getAllCallRecordings: DEBUG - Retrieved campaign details. Returned data:\n";
                print_r($responseObject);
            }

            // Check that we have a valid campaign id
            if ($responseObject['result']['data'][0]['campaignId'] > 0) {
                // Get all applicable call recordings for this campaign
                getCampaignCallRecordings($baseURL, $userId, $password, $responseObject['result']['data'][0]['campaignId'], $startTime, $endTime, $baseDir, $logLevel);

            } else {
                // Invalid Campaign Id returned
                if ($logLevel >= 1)
                    echo "getCampaignCallRecordings: ERROR - Failed to get valid campaign id\n";
            }

            // Force an exit if this is the last campaign in this organisation
            if ($pageNo == $responseObject['result']['total'])
                $exitFlag = TRUE;

        } else {
            // Failed to get this campaign's details
            if ($logLevel >= 1)
                echo "getCampaignCallRecordings: ERROR - Failed to get campaign details\n";
            $exitFlag = TRUE;
        }

    } while ($exitFlag != TRUE);
}

/*
 * Function:        getCampaignCallRecordings
 *
 * Purpose:         Retrieves all relevant call recordings for a specific campaign.
 *
 * Parameters:      $baseURL        Base URL for the API server
 *                  $userId         User Id of the caller (if blank, no authentication is performed)
 *                  $password       Authentication password of the caller
 *                  $campaignId     Numeric Id of the campaign
 *                  $baseDir        Base of directory tree in which to store the recordings
 *                  $logLevel       Logging level setting. 0 = No logging, 1 = Errors only, 2 = Debug, 3 = cURL debug
 *
 * Returns:         On success, the data string returned from the remote server. On error, an empty data string.
 *
 */
function getCampaignCallRecordings($baseURL, $userId, $password, $campaignId, $startTime, $endTime, $baseDir, $logLevel)
{
    // Read the list of all calls during the defined time period for this campaign
    // As with the readCampaignsList API function, the readCallsList API function is designed to return call details details in pages, with a user-defined length.
    // This is useful when building web pages to view the data, but for our purposes, it is more convenient to cycle through the calls
    // one at a time. This is simply achieved by setting the page size to 1, and using the page number as an incrementing index.
    $curlURL = $baseURL . 'campaign/readcallslist';
    $pageNo = 0;
    $exitFlag = FALSE;
    do {
        $pageNo++;
        $curlData = 'pageNo=' . $pageNo . '&perPage=1&sortField=fromTimestamp&sortOrder=ASC&campaignId=' . $campaignId . '&fromTimestamp=' . $startTime . '&toTimestamp=' . $endTime;
        $response = sendRequest($curlURL, 'GET', $userId, $password, $curlData, $logLevel);

        if ($response != '') {
            // Managed to retrieve the calls details. Decode them.
            $responseObject = json_decode($response, TRUE);

            // If we have the details of a real call, then retrieve the recording
            if ($responseObject['resultCode'] == 'success') {
                if ($logLevel >= 2) {
                    echo "getCampaignCallRecordings: DEBUG - Retrieved details of one call. Returned data:\n";
                    print_r($responseObject);
                }

                // Retrieve this call recording, if there is anything to retrieve
                $callDetails = $responseObject['result']['data'][0];
                if (($callDetails['duration'] > 0) && ($callDetails['isDeleted'] == 0)) {
                    // Generate the full filepath of the target file.
                    $filepath = buildFilepath($baseDir, $callDetails, $logLevel);

                    // Check if file already exists. If so, skip this retrieval - we got this file previously
                    if (!file_exists($filepath)) {
                        // Fetch this call recording
                        if (getOneCallRecording($baseURL, $userId, $password, $callDetails['activityId'], $filepath, $logLevel)) {
                            // Successfully retrieved the requested call recording
                            $result = "SUCCESS";
                        } else {
                            // Failed to retrieve the requested call recording
                            $result = "FAILURE";
                        }

                        // Check if the Calls Log CSV file exists. If not, create it, and write the header row
                        $logFile = $baseDir . "CallsLog.csv";
                        if (!file_exists($logFile)) {
                            $headerRow = "Campaign Id, Campaign Title, Activity Id, Interaction Id, Start Timestamp, Duration (secs), Wrap Code, Agent Name, Caller Number, Called Number, Retrieval Timestamp, Retrieval Result\n";
                            $handle = fopen($logFile, 'c');
                            fwrite($handle, $headerRow);
                            fclose($handle);
                        }

                        // Update the Calls Log CSV file with details of the file, and the time and results of the retrieval attempt
                        $retrievalTime = strftime("%F %T", time());
                        $logString = $callDetails['campaignId'] . ',' . $callDetails['campaignTitle'] . ',' . $callDetails['activityId'] . ',' . $callDetails['interactionId'];
                        $logString .= ',' . $callDetails['startTimestamp'] . ',' . $callDetails['duration'] . ',' . $callDetails['wrapCode'] . ',' . $callDetails['agentName'];
                        $logString .= ',' . $callDetails['callerNumber'] . ',' . $callDetails['calledNumber'] . ',' . $retrievalTime . ',' . $result . "\n";
                        $handle = fopen($logFile, 'a');
                        fwrite($handle, $logString);
                        fclose($handle);
                    }
                }

                // Force an exit if this was the last call in this campaign
                if ($pageNo == $responseObject['result']['total'])
                    $exitFlag = TRUE;
                else
                    // Otherwise, sleep for a few seconds to avoid overloading the API server
                    sleep(5);

            } else {
                // If we failed for any reason to get the call details, force an exit (eg there might not be any calls within the defined period for this campaign).
                if ($logLevel >= 2) {
                    echo "getCampaignCallRecordings: DEBUG - No call details returned. Returned data:\n";
                    print_r($responseObject);
                }
                $exitFlag = TRUE;
            }

        } else {
            // Failed to get the requested call details
            if ($logLevel >= 1)
                echo "getCampaignCallRecordings: ERROR - Failed to retrieve call details for campaign id: " . $campaignId . "\n";
            $exitFlag = TRUE;
        }

    } while ($exitFlag != TRUE);
}

/*
 * Function:        getOneCallRecording
 *
 * Purpose:         Retrieves one identified call recording.
 *
 * Parameters:      $baseURL        Base URL for the API server
 *                  $userId         User Id of the caller (if blank, no authentication is performed)
 *                  $password       Authentication password of the caller
 *                  $activityId     Full details of the call to be retrieved, as returned by readcallslist
 *                  $filepath       Full path and filename of the target file
 *                  $logLevel       Logging level setting. 0 = No logging, 1 = Errors only, 2 = Debug, 3 = cURL debug
 *
 * Returns:         TRUE on success, FALSE otherwise
 *
 */
function getOneCallRecording($baseURL, $userId, $password, $activityId, $filepath, $logLevel)
{
    $result = FALSE;
    $curlURL = $baseURL . 'campaign/readcallrecording';
    $curlData = 'activityId=' . $activityId;
    $response = sendRequest($curlURL, 'GET', $userId, $password, $curlData, $logLevel);
    if ($response != '') {
        // Managed to retrieve the call, so save it to disk
        if (file_put_contents($filepath, $response)) {
            $result = TRUE;
            if ($logLevel >= 2)
                echo "getOneCallRecording: DEBUG - Successfully saved call recording for activity id: " . $activityId . "\n";
        } else {
            if ($logLevel >= 1)
                echo "getOneCallRecording: ERROR - Failed to save call recording for activity id: " . $activityId . "\n";
        }

    } else {
        // Something went wrong with the API request.
        if ($logLevel >= 1)
            echo "getOneCallRecording: ERROR - Failed to fetch call recording for activity id: " . $activityId . "\n";
    }

    return $result;
}

/*
 * Function:        buildFilepath
 *
 * Purpose:         Build a full filename (including path) based on the base directory and the call details.
 *
 * Parameters:      $baseDir        Base URL for the API server
 *                  $callDetails    Associative array of the call details retrieved using the readCallsList API call
 *
 * Returns:         The full file path, as a string
 *
 */
function buildFilepath($baseDir, $callDetails, $logLevel)
{
    $filepath = '';

    // There are many schemes that could be used to build the filename and path of the output file. The simplest would be to store
    // them all in one directory. But, this is probably not ideal, as the number of files could grow very large, causing access to slow
    // down and, in the extreme, blowing the operating system's limits on the number of files allowed in one directory.
    // So the approach used here is to store them in separate sub-directories, names after the year and month of the start of the call.

    // Parse the date and time of the start of the call
    $dateArray = date_parse($callDetails['startTimestamp']);

    // Build the sub-directory name, based on the base directory and the year and month
    $dirName = $baseDir . sprintf("%04d_%02d", $dateArray['year'], $dateArray['month']);

    // Check whether the sub-directory already exists. If not, create it
    if (!file_exists($dirName)) {
        // Directory does not exist, so try and create it
        if (!mkdir($dirName)) {
            if ($logLevel >= 1)
                echo "buildFilepath: ERROR - Failed to create directory: " . $dirName . "\n";
        } else {
            if ($logLevel >= 2)
                echo "buildFilepath: DEBUG - Successfully created target directory: " . $dirName . "\n";
        }
    }

    // Build the filename from the Campaign Id, Activity Id, Interaction Id and call start timestamp
    $compressedTimestamp = sprintf("%04d%02d%02d", $dateArray['year'], $dateArray['month'], $dateArray['day']);
    $filepath = $dirName . "\\" . "CR-" . $callDetails['campaignId'] . "-" . $callDetails['activityId'] . "-" . $callDetails['interactionId'] . "_" . $compressedTimestamp . ".mp3";

    return $filepath;
}

?>
