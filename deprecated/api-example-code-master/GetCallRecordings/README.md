# Introduction to GetOrgCallRecordings

GetOrgCallRecordings is example PHP code that provides the logic to retrieve all call recordings, for all campaigns, made during a defined period of time. It does this using API calls to an API server in the ipSCAPE Cloud Contact Centre System. It is intended as an example of how this functionality can be implemented using the ipSCAPE API, rather than as a real-life implementation.

# Prerequsites

- A user account in the Workspace of the relevant ipSCAPE tenant
- An API key for access to the Workspace of the relevant ipSCAPE tenant

# Usage

The application is invoked as follows:

`php GetOrgCallRecordings.php <INI file name>`

In GetOrgCallRecordings.php, please remember to change the ```$baseDir = "D:\\Dev\Data\\recordings\\"; ``` into appropriate path.

where:
- The mandatory INI file name defines the set of user credentials needs to identify the target Workspace, and the user credentials

## INI file format

The INI file contains 4 lines that define the essential credentials needed to log into the API server for a particular tenant, and issue API request. Those credentials are:

| Parameter Name | Purpose |
| -------------- | ------- |
| baseURL | Defines the full API server URL. |
| userId | Identifies the user under whose account API operations will be performed. |
| password | Defines the password of the identifed user. |
| apiKey | Defines the API key to be used for this application. |

An example file is:

```
baseURL=https://xyz.ipscape.com.au/api/latest/
userId=testuser
password=12345678
apiKey=01234567891ab
```

## Operational Parameters

Certain operational parameters are defined at the start of the GetOrgCallRecordings.php module. They are:

| Parameter Variable Name | Purpose |
| ----------------------- | ------- |
| $orgTimezone | Defines the Timezone for the tenant organisation (which is used for setting the search period). |
| $duration | Defines the period, in seconds before now, for which call recordings are required. |
| $baseDir | The base directory path, in which the recordings will be stored. This directory MUST already exist. |

These parameters could easily be provided through the command line but, since they will probably seldom change, they have been left as in-line code.

## Dependencies

The modules in this application include modules from the [Common](https://github.com/ipSCAPE/api-example-code/tree/master/Common) ipSCAPE GitHub repository.

# Operation

The purpose of this example application is to retrieve all call recordings made in the last $duration seconds, before now, and place them in a directory off the $basedir. In addition, the application maintains a CSV-format log file that records the details of the call recordings that have been downloaded.

The structure of the download process is quite simple - the application iterates through all campaigns and, for each, it iterates through every call recording made in that campaign during the prescribed time period. In order to avoid the possibility of overloading the API server, a 5 second sleep occurs after each call recording is processed.

## Target Directory Structure

The downloaded call recordings are placed in a month-by-month directory below the $basedir. The name of the sub-directory is constructed from the year and month of the start of the call in the format "YYYY_MM". If this directory doesn't exist when it is needed, it will be created by the application.

## Downloaded Filenames

The downloaded call recordings are given a name based on the details of the call, structured as follows:

` CR-<Campaign Id>-<Activity Id>-<Interaction Id>.mp3`

For example:

` CR-1017-217962-39284_20160201 `

If a file of this name already exists in the target directory, the application assumes the call recording has already been downloaded, and skips it.

## CSV-format Log File

### Location

The CSV-format Log File is constructed in the $basedir, and is given the name CallsLog.csv. It is a running log, so successive runs of the application will simply add further rows to the file. On first execution of the application, if the CSV file doesn't already exist, it will be created.

### Contents

The CSV-format Log File follows the standard CSV file format, with the first row holding field names, and subsequent rows holding data. The fields recorded are:

| Field | Meaning |
| ----- | ------- |
| Campaign Id | The numeric Id of the campaign in which this call was made. |
| Campaign Title | The alphanumeric name of the campaign in which this call was made.|
| Activity Id | The numeric Activity Id of the call. This is not necessarily unique, if the call was transferred between agents. |
| Interaction Id | The numeric Interaction Id of the call. This is not necessarily unique, if the call was one of several made on a Preview campaign for the same Lead. |
| Start Timestamp | The date and time stamp of the start of the call. |
| Duration (secs) | The duration of the call, in seconds. |
| Wrap Code | The numeric wrap code assigned by the agent to this call. |
| Agent Name | The name of the agent who took/made this call. |
| Caller Number | The phone number of the caller (CLI), in E.164 format. |
| Called Number | The phone number of the called party, in E.164 format. |
| Retrieval Timestamp | The date and time stamp of the time at which this attempt to retrieve the call recording was made. |
| Retrieval Result | A text string explainging the success or failure of the retrieval attempt. |

# Licence

This project has been released under a modified version of the [MIT License](https://opensource.org/licenses/MIT), the text of which is included below. The only modification is the removal of the obligation to include the standard permission notice in any copies of the Software. This license applies ONLY to the source of this repository and does not extend to any other ipSCAPE distributions or variants, or any other 3rd party libraries used in a repository.

> Copyright (c) 2016 ipSCAPE Pty Limited.

> Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
  to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
  and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so.

> THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
  IN THE SOFTWARE.
