<?php

require_once dirname(__FILE__) . '/sendrequest.php';

/*
 * Module:          session.php
 *
 * Purpose:         Provides functions to log in and log out of the ipSCAPE API.
 *
 * Author:          Simon Laing
 *
 * Copyright:       Copyright (c) 2018 ipSCAPE Pty Limited.
 *
 *                  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 *                  to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 *                  and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so.
 *
 *                  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *                  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *                  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 *                  IN THE SOFTWARE.
 *
 * Revision History
 *      4/1/2016    SGL     First version.
 *     15/6/2018    OL      Refactoring.
 */

/*
 * Function:        apiLogin
 *
 * Purpose:         Attempts to log in to the API.
 *
 * Parameters:      $baseURL        Base URL, which does not include the function-specific directory information.
 *                  $userId         User Id of the caller
 *                  $password       Authentication password of the caller
 *                  $apiKey         The API key used to authenitcate the calling application
 *                  $logLevel       Logging level setting. 0 = No logging, 1 = Errors only, 2 = Debug, 3 = cURL debug
 *
 * Returns:         On success, the data string returned from the remote server. On error, an empty data string.
 *
 */
function apiLogin($baseURL, $userId, $password, $apiKey, $logLevel)
{
    // Log in to the API
    $curlURL = $baseURL . 'user/login';
    $curlData = array('apiKey' => $apiKey);
    $response = sendRequest($curlURL, 'POST', $userId, $password, $curlData, $logLevel);

    if ($response != '') {
        // Successfully sent the login request, but check whether login was successful?
        $responseObject = json_decode($response, TRUE);
        if ($responseObject['resultCode'] == 'success') {
            if ($logLevel >= 2) {
                echo "DEBUG - Login succeeded. Returned data:\n";
                print_r($responseObject);
            }

        } else {
            // The login request failed. Maybe the password is wrong?
            if ($logLevel >= 1) {
                echo "apiLogin: ERROR - Login failed. Returned data:\n";
                print_r($responseObject);
            }
            // Clear the reply data to report the error
            $response = '';
        }
    } else {
        // Failed to send the login request to the API
        if ($logLevel >= 1)
            echo "apiLogin: ERROR - Login request failed.\n";
    }

    return $response;
}

/*
 * Function:        apiLogout
 *
 * Purpose:         Attempts to log out from the API.
 *
 * Parameters:      $baseURL        Base URL, which does not include the function-specific directory information.
 *                  $userId         User Id of the caller
 *                  $password       Authentication password of the caller
 *                  $logLevel       Logging level setting. 0 = No logging, 1 = Errors only, 2 = Debug, 3 = cURL debug
 *
 * Returns:         On success, the data string returned from the remote server. On error, an empty data string.
 *
 * Author:          Simon Laing
 *
 * Revision History
 *      4/1/2016     SGL     First version.
 *      15/6/2018    OL      Refactoring.
 */
function apiLogout($baseURL, $userId, $password, $logLevel)
{
    // Log out from the API
    $curlURL = $baseURL . 'user/logout';
    $response = sendRequest($curlURL, 'POST', $userId, $password, '', $logLevel);

    if ($response != '') {
        // Decode the response data
        $responseObject = json_decode($response, TRUE);

        // Managed to log out
        if ($logLevel >= 2) {
            echo "apiLogout: DEBUG - Logged out of the API. Returned data:\n";
            print_r($responseObject);
        }

    } else {
        // Failed to log out of the API
        if ($logLevel >= 1)
            echo "apiLogout: ERROR - Failed to log out of the API.\n";
    }
}

?>

