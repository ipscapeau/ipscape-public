<?php
/*
 * Module:          sendrequest.php
 *
 * Purpose:         Provides (one) function to send an HTTP request, and get the reply back. All methods (GET, POST, PUT, DELETE, etc) are supported.
 *
 * Author:          Simon Laing
 *
 * Copyright:       Copyright (c) 2018 ipSCAPE Pty Limited.
 *
 *                  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 *                  to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 *                  and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so.
 *
 *                  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *                  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *                  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 *                  IN THE SOFTWARE.
 *
 * Revision History
 *      3/1/2016    SGL     First version. Derived from class.ACC_curl.php
 *     15/6/2018    OL      Refactoring
 */

/*
 * Function:        sendRequest
 *
 * Purpose:         Sends an HTTP request, to a remote server, and get the reply back. All methods (GET, POST, PUT, DELETE, etc) are supported.
 *
 * Parameters:      $URL            Endpoint URL
 *                  $method         HTTP method to use. Typically GET or POST, but a number of others are defined (see http://www.w3.org/Protocols/HTTP/Methods.html)
 *                  $userId         User Id of the caller (if blank, no authentication is performed)
 *                  $password       Authentication password of the caller
 *                  $data           Data to be sent. This can be a urlencoded string, or an array, if the POST method is used.
 *                  $logLevel       Logging level setting. 0 = No logging, 1 = Errors only, 2 = Debug, 3 = cURL debug
 *
 * Returns:         On success, the data string returned from the remote server. On error, an empty data string.
 *
 */

function sendRequest($URL, $method, $userId, $password, $data, $logLevel = 0)
{
    // Array of cURL options, with standard defaults already set.
    $arrOptions = array(
        CURLOPT_AUTOREFERER => FALSE,            // Set referer on redirect
        CURLOPT_CONNECTTIMEOUT => 5,                // Timeout on connect
        CURLOPT_ENCODING => "",               // Handle all encodings
        CURLOPT_FAILONERROR => FALSE,            // Fail verbosely if HTTP return code >= 400
        CURLOPT_FOLLOWLOCATION => FALSE,            // Don't follow redirects
        CURLOPT_HEADER => FALSE,            // Don't return headers
        CURLOPT_MAXREDIRS => 10,               // Maximum number of re-directs
        CURLOPT_RETURNTRANSFER => TRUE,             // Return web page
        CURLOPT_VERBOSE => FALSE,            // By default, don't log cURL operations
        CURLOPT_SSL_VERIFYHOST => FALSE,            // Don't verify SSL
        CURLOPT_SSL_VERIFYPEER => FALSE,            // Don't check SSL
        CURLOPT_TIMEOUT => 120,              // Timeout (in seconds) on response
        CURLOPT_URL => '',               // URL to send the request to. Cannot be blank
        CURLOPT_USERAGENT => '',               // Who am I
    );

    $hCurl;                                         // Handle for the cURL operations
    $content = '';                                  // For any data returned by the cURL request
    $info = array();                               // For the status results of the cURL request

    // Open a cURL session
    $hCurl = curl_init();
    if ($hCurl !== FALSE) {

        // set headers
        // $arrOptions[CURLOPT_HTTPHEADER] = array('X-ipscape-org: preprod');

        // If cURL debug logging is enabled, then override the verbosity setting
        if ($logLevel >= 3)
            $arrOptions[CURLOPT_VERBOSE] = TRUE;

        // If a non-blank user id is provided, then use Basic Authorization
        if ($userId != '')
            $arrOptions[CURLOPT_USERPWD] = $userId . ":" . $password;

        // Set up the HTTP method, as requested
        if ($method === 'GET') {
            // The default - use the GET method. That means any data is appended to the URL
            $arrOptions[CURLOPT_HTTPGET] = 1;
            if ($data != '')
                $arrOptions[CURLOPT_URL] = $URL . '?' . $data;
            else
                $arrOptions[CURLOPT_URL] = $URL;
        } else {
            // In all other cases, set up the full URL and put the data in the POSTFIELDS
            $arrOptions[CURLOPT_URL] = $URL;

            // Check the type of the data provided. If it's an array, it will need to be encoded.
            if (gettype($data) == 'array')
                $arrOptions[CURLOPT_POSTFIELDS] = http_build_query($data);
            else
                $arrOptions[CURLOPT_POSTFIELDS] = $data;

            if ($method === 'POST') {
                // Select POST as the method
                $arrOptions[CURLOPT_POST] = 1;
            } else {
                // Otherwise, just pick up the custom request type
                $arrOptions[CURLOPT_CUSTOMREQUEST] = $method;
            }
        }
        // Set up the options
        if (curl_setopt_array($hCurl, $arrOptions) === FALSE) {
            // Failed to set options, so report the error to the console
            if ($logLevel >= 1)
                echo "sendRequest: ERROR - Failed to set cURL options.\n";

        } else {
            if ($logLevel >= 2) {
                echo "sendRequest: DEBUG - Options for cURL request:\n";
                print_r($arrOptions);
            }

            // Now issue the cURL request, and accept any reply data
            $content = curl_exec($hCurl);

            // Get cURL info, including the HTTP response code
            $info = curl_getinfo($hCurl);

            // Log an error if the HTPP response code is not in the 200 - 299 range
            if ((int)($info['http_code'] / 100) != 2) {
                // Report the last error to the console, and any data that was returned.
                if ($logLevel >= 1)
                    echo "sendRequest: ERROR - HTTP request failed. HTTP error: " . $info['http_code'] . ", curl_exec error: " . curl_error($hCurl) . "\n";
                if ($content != '') {
                    echo "sendRequest: ERROR - Data returned: \n";
                    print_r(json_decode($content, TRUE));
                }

                // Clear out the content returned, so the caller knows the data cannot be used.
                $content = '';
            }
        }

        // Close the curl session
        curl_close($hCurl);

    } else {
        // Failed to initiate a cURL session. Should only ever occur if cURL is not enabled in php.ini
        if ($logLevel >= 1)
            echo "sendRequest: ERROR - Failed to initialise cURL session\n";
    }

    // Return result
    return $content;
}

?>