# Introduction

This folder contains a number of files used by other modules within the [ipSCAPE API Example Code](https://bitbucket.org/ipscapeau/ipscape-public/src/master/api/) Bitbucket repository. It contains the following files:

| Filename | Purpose |
| -------- | ------- |
| activites.txt | list of activities used by readcallrecordings.php |
| config.ini | Provides an example credentials file, that is required to use the example applications |

# Licence

This project has been released under a modified version of the [MIT License](https://opensource.org/licenses/MIT), the text of which is included below. The only modification is the removal of the obligation to include the standard permission notice in any copies of the Software. This license applies ONLY to the source of this repository and does not extend to any other ipSCAPE distributions or variants, or any other 3rd party libraries used in a repository.

> Copyright (c) 2018 ipSCAPE Pty Limited.

> Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
  to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
  and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so.

> THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
  IN THE SOFTWARE.
