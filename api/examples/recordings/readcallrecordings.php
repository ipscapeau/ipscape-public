<?php

require_once dirname(__FILE__) . '/../common/sendrequest.php';
require_once dirname(__FILE__) . '/../common/session.php';

// Parse the command line, looking for the filename of the ini file containing the operational parameters
if ($argc != 3) {
    echo "Syntax is: php readcallrecordings.php ./../config/config.ini ./../config/activities.txt \n";
    exit;

} else {
    // Attempt to open the ini file, and extract the operational parameters
    $params = parse_ini_file($argv[1]);
    if (!$params) {
        // Failed to parse the INI file, so force an exit
        if ($logLevel >= 1) {
            echo "readcallrecordings: ERROR - Failed to parse INI file: " . $argv[1] . "\n";
        }

    } else {
        // All OK, so attempt to log in to the API
        $logLevel = $params['logLevel'];
        $response = apiLogin($params['baseURL'], $params['userId'], $params['password'], $params['apiKey'], $logLevel);

        if ($response != '') {
            // Get all call recordings according to the input list
            $fh = fopen($argv[2], 'r');
            while ($activity = fgets($fh)) {
                echo("Downloading call recording for activity id: " . $activity);
                readCallRecording($params['baseURL'], $params['userId'], $params['password'], rtrim($activity), $params['downloadDir'], $logLevel);
            }
            fclose($fh);

            // Now log out of the API
            apiLogout($params['baseURL'], $params['userId'], $params['password'], $logLevel);
        }
    }
}

function readCallRecording($baseURL, $userId, $password, $activityId, $downloadDir, $logLevel)
{
    $result = FALSE;
    $curlURL = $baseURL . 'campaign/readcallrecording';
    $curlData = 'activityId=' . $activityId;
    $response = sendRequest($curlURL, 'GET', $userId, $password, $curlData, $logLevel);

    if ($response != '') {

        // Managed to retrieve the call, so save it to disk
        if (file_put_contents($downloadDir . $activityId . ".mp3", $response)) {
            $result = TRUE;
            if ($logLevel >= 2)
                echo "readCallRecording: DEBUG - Successfully saved call recording for activity id: " . $activityId . "\n";
        } else {
            if ($logLevel >= 1)
                echo "readCallRecording: ERROR - Failed to save call recording for activity id: " . $activityId . "\n";
        }

    } else {
        // Something went wrong with the API request.
        if ($logLevel >= 1)
            echo "readCallRecording: ERROR - Failed to fetch call recording for activity id: " . $activityId . "\n";
    }

    return $result;
}

?>
