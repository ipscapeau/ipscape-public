# Introduction to readcallrecordings.php

readcallrecordings.php is example PHP code that provides the logic to retrieve call recording from specified in the activities.txt list.
It does this using curl calls to the ipSCAPE API. It is intended as an example of how this functionality can be implemented using the ipSCAPE API, rather than as a real-life implementation.

# Prerequsites

- A user account in the Workspace of the relevant ipSCAPE tenant
- An API key for access to the Workspace of the relevant ipSCAPE tenant
- php (with php-curl module)

# Usage

The application is invoked as follows:

`php readcallrecordings.php ./../config/config.ini ./../config/activities.txt`

## INI file format

The INI file contains 6 lines that define the essential credentials and other configuration used by the API code examples:

| Parameter Name | Purpose |
| -------------- | ------- |
| baseURL | Defines the full API server URL. |
| userId | Identifies the user under whose account API operations will be performed. |
| password | Defines the password of the identifed user. |
| apiKey | Defines the API key to be used for this application. |
| baseDir | Path to save the call recordings. |
| logLevel | Logging granularity: 0 = No logging, 1 = Errors only, 2 = Debug, 3 = cURL debug. |

An example config.ini file is:

```
baseURL=https://xyz.ipscape.com.au/api/latest/
userId=testuser
password=12345678
apiKey=01234567891ab
baseDir = /home/user/Downloads/recordings/
logLevel = 3; 
```

## Dependencies

The modules in this application include modules from the [ipSCAPE API Example Code](https://bitbucket.org/ipscapeau/ipscape-public/src/master/api/) Bitbucket repository.

# Licence

This project has been released under a modified version of the [MIT License](https://opensource.org/licenses/MIT), the text of which is included below. The only modification is the removal of the obligation to include the standard permission notice in any copies of the Software. This license applies ONLY to the source of this repository and does not extend to any other ipSCAPE distributions or variants, or any other 3rd party libraries used in a repository.

> Copyright (c) 2018 ipSCAPE Pty Limited.

> Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
  to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
  and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so.

> THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
  IN THE SOFTWARE.
